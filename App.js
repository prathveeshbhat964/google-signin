import React, { useEffect, useState } from 'react';
import { StatusBar, Pressable, StyleSheet, Text, View, Image } from 'react-native';
import * as WebBrowser from "expo-web-browser";
import * as Google from 'expo-auth-session/providers/google';
import AsyncStorage from '@react-native-async-storage/async-storage';

WebBrowser.maybeCompleteAuthSession();

export default function App() {
  const [userInfo, setUserInfo] = useState(null);

  const [request, response, promptAsync] = Google.useAuthRequest({
    webClientId: "634010584176-ov40tp7nulv2ll3e86nsil9kcnhpdkdi.apps.googleusercontent.com",
    androidClientId: "634010584176-smddb4e1pq4fidmu08s7bp92n60e0oos.apps.googleusercontent.com",
    iosClientId: "634010584176-smddb4e1pq4fidmu08s7bp92n60e0oos.apps.googleusercontent.com",
  });

  useEffect(() => {
    handleSignInWithGoogle();
  }, [response]);

  async function handleSignInWithGoogle() {
    const user = await AsyncStorage.getItem("@user");
    if (!user) {
      if (response?.type === "success") {
        await getUserInfo(response.authentication.accessToken);
      }
    } else {
      setUserInfo(JSON.parse(user));
    }
  }

  const getUserInfo = async (token) => {
    if (!token) return;
    try {
      const response = await fetch(
        "https://www.googleapis.com/userinfo/v2/me",
        {
          headers: { Authorization: `Bearer ${token}` },
        }
      );
      const user = await response.json();
      await AsyncStorage.setItem("@user", JSON.stringify(user));
      setUserInfo(user);
    }
    catch (error) {
      console.error(error);
    }
  };

  const handleDeleteUserInfo = async () => {
    try {
      await AsyncStorage.removeItem("@user");
      setUserInfo(null);
    } catch (error) {
      console.error(error);
    }
  };

  return (
    <View style={styles.container}>
      {userInfo && (
        <View style={styles.userInfoContainer}>
          <Image source={{ uri: userInfo.picture }} style={styles.userImage} />
          <View style={styles.userInfo}>
            <Text>Name: {userInfo.name}</Text>
            <Text>Email: {userInfo.email}</Text>
            <Text>Locale: {userInfo.locale}</Text>
            {/* Add more fields as needed */}
          </View>
        </View>
      )}
      <Pressable style={styles.button} onPress={() => promptAsync()} ><Text style={styles.buttonText}>Sign in with Google</Text></Pressable>
      <Pressable style={styles.button} onPress={()=>handleDeleteUserInfo()}><Text style={styles.buttonText}>Signout</Text></Pressable>

      <StatusBar style="dark" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  userInfoContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 20,
    marginLeft:70,
  },
  userImage: {
    width: 50,
    height: 50,
    borderRadius: 25,
    marginRight: 10,
  },
  userInfo: {
    flex: 1,
    //alignItems:"center",
    //justifyContent:"center",
  },
  button: {
    backgroundColor: 'black',
    padding: 10,
    borderRadius: 5,
    marginVertical: 10,
    width:150,
    alignItems:'center',
    height:40,
  },
  buttonText: {
    color: '#fff',
    fontWeight: 'bold',
  }
});
